#!/bin/bash

rm -rf ~/Homestead/after.sh
rm -rf ~/Homestead/Homestead.yaml
ln -s ~/.dotfiles/Homestead/after.sh ~/Homestead/after.sh 
ln -s ~/.dotfiles/Homestead/Homestead.yaml ~/Homestead/Homestead.yaml